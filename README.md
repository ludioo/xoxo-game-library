# Dokumentasi

Beberapa class yang ada dalam library :
* Class Player          =   Digunakan untuk mengatur behavior dan attribute pada Objek pemain
* Class Board           =   Digunakan untuk membuat Map pada game
* Class BoardInput      =   Digunakan untuk mengatur inputan apa saja yang akan dimasukkan oleh pemain
* Class CommandManager  =   Digunakan untuk mengimplementasikan Command pattern yang berfungsi untuk Undo pada Game
* Class GameManager     =   Digunakan untuk mengatur keseluruhan behavior yang ada pada Game

# Class Player

Beberapa attribute yang ada pada Class Player adalah :
* String namaInit   =   Digunakan untuk menyimpan nama pemain dalam bentuk string
* Char bidakInit    =   Digunakan untuk menyimpan bentuk bidak yang pemain masukkan
* Bool turnInit     =   Digunakan untuk mengatur giliran masing-masing player

Class Player memiliki penggunaan melalui Constructor 
* Pengguna dapat secara langsung melakukan costumisasi nilai dalam game melalui Constructor class, dengan memperhatikan parameter yang ada (string namaInt), (char bidakInit) , (bool turnInit)
<img src = "Image/Player.png" width = "500">

# Class Board

Beberapa attribute yang ada pada Class Board adalah :
* int x                 =   Digunakan untuk nilai panjang
* int y                 =   Digunakan untuk nilai lebar
* char bidak            =   Digunakan untuk menyimpan bidak
* char map[ ][ ]        =   Digunakan untuk mengatur Array 2 Dimensi pada Map

Class Board memiliki penggunaan melalui Getter and Setter
* Pengguna harus menggunakan fungsi Getter and Setter yang telah di sediakan
<img src = "Image/Board2.png" width = "500">
<img src = "Image/Board.png" width = "500">

# Class BoardInput

Beberapa attribute yang ada pada Class BoardInput adalah :
* Board *board  =   Pointer kepada class Board
* int x         =   Digunakan untuk memasukkan nilai panjang
* int y         =   Digunakan untuk memasukkan nilai lebar
* char bidak    =   Digunakan untuk memasukkan bidak kedalam board

Class BoardInput memiliki penggunaan melalui Constructor :
* Pengguna dapat secara langsung melakukan costumisasi nilai dalam game melalui Constructor class, dengan memperhatikan parameter yang ada (Board *board), (int x) , (int y), (char bidak)
<img src = "Image/BoardInput.png" width = "500">

# Class CommandManager

Attribute yang ada pada Class CommandManager adalah Objek BoardInput, dikarenakan dia berfungsi sebagai wadah untuk menampung list yang ada pada Class BoardInput

Class CommandManager memiliki penggunaan melalui Constructor :
* Pengguna dapat secara langsung melakukan penggunaan dengan memperhatikan parameter yang ada yaitu pada objek BoardInput
<img src = "Image/CommandManager.png" width = "500">

# Class GameManager

Class GameManager merupakan yang mengatur keseluruhan objek pada Game, seperti contohnya :
* InitGame()        = berfungsi untuk inisialiasi game
* PrintGame()       = berfungsi untuk mencetak board game
* BoardInputCmd()   = berfungsi mencatat inputan pemain pada board
* UndoInput()       = berfungsi mencatat inputan pemain pada proses Undo
* AdvanceTurn()     = berfungsi untuk mengganti giliran masing-masing pemain
* GameOver()        = berfungsi untuk mengetahui bahwa game telah selesai
* GetPoint()        = berfungsi menyimpan point masing-masing pemain
* GetIsPlaying()    = berfungsi untuk mengetahu bahwa game sedang berlangsung

Class GameManager memiliki penggunaan melalui beberapa fungsi yang telah disediakan
* seperti pada gambar yang ada di bawah ini
<img src = "Image/GameManager.png" width = "500">
