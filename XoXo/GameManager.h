#pragma once
#include "Player.h"
#include "Board.h"
#include "CommandManager.h"



class GameManager
{
public:
	GameManager();
	~GameManager();
	
	//Deklarasi class Player
	Player p1;
	Player p2;

	//Deklarasi class Board
	Board board;

	//Deklarasi class CommandManager
	CommandManager cmdManager;

	//Fungsi ketika Game selesai
	void GameOver();
	//Fungsi permulaan Game
	void InitGame();
	//Fungsi cetak Game
	void PrintGame();
	//Fungsi Pengaturan giliran
	void AdvanceTurn();
	//Fungsi untuk inputan board
	void BoardInputCmd(int x, int y);
	//Fungsi untuk inputan undo di board
	void UndoInput();

	//Fungsi check horizontal pada board
	bool checkHoriz(Player*, int,int);
	//Fungsi check vertical pada board
	bool checkVerti(Player*, int,int);
	//Fungsi check point masing-masing pemain
	int CheckPoint(Player *cp);
	//Fungsi mengecheck apakah board telah terisi full
	int CheckFull();

	//Boolean ketika game berjalan
	bool GetIsPlaying();
	//Untuk menghitung round yang berjalan
	int GetRound();

private:
	bool isPlaying;
	int round;

};

