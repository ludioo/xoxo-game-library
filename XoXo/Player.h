#pragma once
#include <iostream>
#include <string.h>

//Erieca Faradina (4210171009)

class Player
{
public:
	Player();
	
	//Operator Overloading pada Class player
	Player(std::string namaInit, char bidakInit, bool turnInit);
	~Player();

	//Fungsi set Nama
	void SetNama(std::string namaBaru);
	//Fungsi set Bidak
	void SetBidak(char bidakBaru);
	//Fungsi set point
	void SetPoint(int pointBaru);
	//Fungsi set giliran
	void SetTurn(bool turnBaru);

	std::string GetNama();
	char GetSymbol();
	int GetPoint();
	bool GetTurn();



private:
	int point;
	char bidak;
	std::string nama;
	bool isTurn;
};

