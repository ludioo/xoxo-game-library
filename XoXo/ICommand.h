#pragma once
#include "pch.h"
#include <memory>
#include <stack>

// M Barkah Rahmaanu M (4210171022)
class ICommand
{
public:
	//Fungsi Eksekusi = 0
	virtual void execute() = 0;
	//Fungsi Undo = 0
	virtual void undo() = 0;
};

