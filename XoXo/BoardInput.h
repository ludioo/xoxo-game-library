#pragma once
#include "ICommand.h"
#include "Board.h"
// Candra Wira Mahendra (4210171012)

class BoardInput : public ICommand
{
	//Pointer board
	Board *mBoard;
	char mOldBidak;
	int mOldX;
	int mOldY;
	char mNewBidak;
	int mNewX;
	int mNewY;

public:
	BoardInput(Board *board, int x, int y, char bidak);
	~BoardInput();

	void execute();
	void undo();
};

