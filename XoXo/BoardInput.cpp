#include "pch.h"
#include "BoardInput.h"

// Candra Wira Mahendra (4210171012)

BoardInput::BoardInput(Board *board, int x, int y, char bidak)
{

	mBoard = board;
	mNewBidak = bidak;
	mNewX = x;
	mNewY = y;
}


BoardInput::~BoardInput()
{
}

//Penggunaan fungsi Input untuk execute
void BoardInput::execute()
{
	mOldBidak = mBoard->GetMap(mNewX, mNewY);
	mOldX = mNewX;
	mOldY = mNewY;
	mBoard->SetMap(mNewX, mNewY, mNewBidak);
}

//Penggunaan fungsi input untuk undo
void BoardInput::undo()
{
	mBoard->SetMap(mOldX, mOldY, mOldBidak);
}
