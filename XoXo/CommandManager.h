#pragma once
#include <stack>
#include <memory>
#include "ICommand.h"

// M Barkah Rahmanu M (4210171022)

typedef std::stack <std::shared_ptr<ICommand>> commandStack;

class CommandManager
{
public:
	CommandManager();
	~CommandManager();
	commandStack undoStack;

	// Mengeksekusi perintah
	void ExecuteCmd(std::shared_ptr<ICommand> command);
	// Menarik perintah
	void Undo();
};

