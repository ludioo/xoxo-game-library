#include "pch.h"
#include "Player.h"

//Erieca Faradina (4210171009)


Player::Player()
{
}

//Operator Overloading pada class Player
Player::Player(std::string namaInit, char bidakInit, bool turnInit)
{
	bidak = bidakInit;
	nama = namaInit;
	isTurn = turnInit;
}


Player::~Player()
{
}

//Setter nama
void Player::SetNama(std::string namaBaru)
{
	nama = namaBaru;
}

//Setter bidak
void Player::SetBidak(char bidakBaru)
{
	bidak = bidakBaru;
}

//Setter point
void Player::SetPoint(int pointBaru)
{
	point = pointBaru;
}

//Setter turn
void Player::SetTurn(bool turnBaru)
{
	isTurn = turnBaru;
}

//Getter nama
std::string Player::GetNama()
{
	return nama;
}

//Getter Symbol
char Player::GetSymbol()
{
	return bidak;
}

//Getter point
int Player::GetPoint()
{
	return point;
}

//Getter giliran
bool Player::GetTurn()
{
	return isTurn;
}
