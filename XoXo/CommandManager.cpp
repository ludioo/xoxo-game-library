#include "pch.h"
#include "CommandManager.h"

// M Barkah Rahmanu M (4210171022)

CommandManager::CommandManager()
{
}


CommandManager::~CommandManager()
{
}

//Penggunaan Fungsi Execute pada Class CommandManager
void CommandManager::ExecuteCmd(std::shared_ptr<ICommand> command)
{
	command->execute();
	undoStack.push(command);
}

//Penggunaan fungsi Undo pada class CommandManager
void CommandManager::Undo()
{
	if (undoStack.size() <= 0)
	{
		return;
	}
	//Stack CommandManager
	undoStack.top()->undo();
	undoStack.pop();
}
