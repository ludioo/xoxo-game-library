#include "pch.h"
#include "GameManager.h"
#include <iostream>;

int main()
{
	//Deklarasi object class GameManager
	GameManager gm;
	char a;
	gm.InitGame();
	while (gm.GetIsPlaying())
	{
		int xInput, yInput;
		char satisfied;
		do
		{
			do
			{
				gm.PrintGame();
				std::cout << "\nBaris ke: ";
				std::cin >> xInput;
				std::cout << "\nKolom ke: ";
				std::cin >> yInput;
			} while (gm.board.GetMap(xInput, yInput) != ' ');
			gm.BoardInputCmd(xInput, yInput);
			gm.PrintGame();
			std::cout << "\nFilled spaces: " << gm.CheckFull();
			std::cout << "\n\nYakin? (y/n): ";
			std::cin >> satisfied;
			if (satisfied == 'n') {
				gm.UndoInput();
			}

			gm.CheckPoint(&gm.p1);
			gm.CheckPoint(&gm.p2);

		} while (satisfied == 'n');



		
		std::cout << "\n" << gm.p1.GetPoint();

		//std::cin >> a;

		gm.AdvanceTurn();

	}
	gm.GameOver();
}