#include "pch.h"
#include "Board.h"
#include <iostream>
// Candra Wira Mahendra (4210171012)

Board::Board()
{
}


Board::~Board()
{
}


//Penggunaan fungsi PrintMap
void Board::PrintMap()
{
	for (int i = 0; i < 11; i++)
	{
		for (int j = 0; j < 11; j++)
		{
			std::cout << map[i][j] << " ";
		}
		std::cout << std::endl;
	}
}

//Setter dan Getter fungsi untuk map
void Board::SetMap(int x, int y, char bidak)
{
	map[x * 2 - 1][y * 2 - 1] = bidak;
}

char Board::GetMap(int x, int y)
{
	return map[x * 2 - 1][y * 2 - 1];
}
	