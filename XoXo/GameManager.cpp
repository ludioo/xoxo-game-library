#include "pch.h"
#include <stdlib.h>
#include <iostream>
#include <string>
#include "GameManager.h"
#include "BoardInput.h"

// M Barkah Rahmanu M (4210171022)


GameManager::GameManager()
{
}


GameManager::~GameManager()
{
}

//Penggunaan fungsi untuk GameOver
void GameManager::GameOver()
{
	system("CLS");
	std::cout << "===================================="
		<< std::endl << "|         ->> CONGRATULATION <<-        |"
		<< std::endl << "===================================="
		<< std::endl << std::endl;
	if (CheckPoint(&p1) > CheckPoint(&p2)) {
		std::cout << p1.GetNama() << " Wins!";
	}
	else if (CheckPoint(&p1) < CheckPoint(&p2)) {
		std::cout << p2.GetNama() << " Wins!";
	}
	else {
		std::cout << "It's a Draw!";
	}
}

//Penggunaan fungsi untuk InitGame
void GameManager::InitGame()
{
	system("CLS");
	std::cout << "-= Game Initialize =-\n\n";
	std::string tempString;
	char tempChar;

	std::cout << "Insert Player 1's Name: ";
	std::cin >> tempString;
	p1.SetNama(tempString);
	std::cout << "\nMasukkan Karakter yang akan dipakai sebagai Bidak: ";
	std::cin >> tempChar;
	p1.SetBidak(tempChar);

	std::cout << "\nInsert Player 2's Name: ";
	std::cin >> tempString;
	p2.SetNama(tempString);
	std::cout << "\nMasukkan Karakter yang akan dipakai sebagai Bidak: ";
	std::cin >> tempChar;
	p2.SetBidak(tempChar);

	p1.SetTurn(true);
	isPlaying = true;

}

//Penggunaan fungsi untuk PrintGame
void GameManager::PrintGame()
{
	system("CLS");
	std::cout << "======|| 4 in A Row ||======\n\n";
	std::cout << "          Round " << round << std::endl << std::endl;
	std::cout << "\n-=========================-\n";
	std::cout << "Score player " << p1.GetNama() << " : " << p1.GetPoint() << std :: endl;
	std::cout << "Score player " << p2.GetNama() << " : " << p2.GetPoint() << std::endl;
	if (p1.GetTurn()) { std::cout << p1.GetNama() << "'s Turn\n\n"; }
	else { std::cout << p2.GetNama() << "'s Turn\n\n"; }

	Board board;
	board.PrintMap();
}

//Fungsi untuk mengatur giliran
void GameManager::AdvanceTurn()
{
	p1.SetTurn(!p1.GetTurn());
	p2.SetTurn(!p2.GetTurn());
	round++;
}

//Fungsi untuk inputan kepada board
void GameManager::BoardInputCmd(int x, int y)
{
	if (p1.GetTurn()) {
		std::shared_ptr<ICommand> p1Cmd(new BoardInput(&board, x, y, p1.GetSymbol()));
		cmdManager.ExecuteCmd(p1Cmd);
	}
	else {
		std::shared_ptr<ICommand> p2Cmd(new BoardInput(&board, x, y, p2.GetSymbol()));
		cmdManager.ExecuteCmd(p2Cmd);
	}
}

//Penggunaan fungsi Input Undo
void GameManager::UndoInput()
{
	cmdManager.Undo();
}

//Penggunaan Fungsi Check bidak horizontal
bool GameManager::checkHoriz(Player *cp, int startIndex, int index) {
	bool status = true;

	for (int i = startIndex; i < startIndex + 4; i++)
	{
		if (board.GetMap(index, i) != cp->GetSymbol()) {
			status = false;
			break;
		}
	}

	return status;
}

//Penggunaan Fungsi Check bidak vertical
bool GameManager::checkVerti(Player *cp, int startIndex,int index) {
	bool status = true;

	for (int i = startIndex; i < startIndex + 4; i++)
	{
		if (board.GetMap(i, index) != cp->GetSymbol()) {
			status = false;
			break;
		}
	}

	return status;
}

//Penggunaan Fungsi untuk mengecheck point untuk menambah point
int GameManager::CheckPoint(Player *cp)
{
	int totalPoints = 0;
	int startIndex = 1;

	for (int i = 1; i <= 4; i++)
	{
		if (checkHoriz(cp, 1, i)) {
			totalPoints++;
		}

		if (checkHoriz(cp, 2, i)) {
			totalPoints++;
		}

		if (checkVerti(cp, 1, i))
		{
			totalPoints++;
		}

		if (checkVerti(cp,2,i))
		{
			totalPoints++;
		}
	}

	//Mengecheck kondisi point saat diagonal
	//Diagonal Check
	if ((board.GetMap(1, 1) == cp->GetSymbol() && board.GetMap(2, 2) == cp->GetSymbol() && board.GetMap(3, 3) == cp->GetSymbol() && board.GetMap(4, 4) == cp->GetSymbol())) {totalPoints++;}
	if ((board.GetMap(2, 2) == cp->GetSymbol() && board.GetMap(3, 3) == cp->GetSymbol() && board.GetMap(4, 4) == cp->GetSymbol() && board.GetMap(5, 5) == cp->GetSymbol())) { totalPoints++; }
	if ((board.GetMap(1, 5) == cp->GetSymbol() && board.GetMap(2, 4) == cp->GetSymbol() && board.GetMap(3, 3) == cp->GetSymbol() && board.GetMap(4, 2) == cp->GetSymbol())) { totalPoints++; }
	if ((board.GetMap(2, 4) == cp->GetSymbol() && board.GetMap(3, 3) == cp->GetSymbol() && board.GetMap(4, 2) == cp->GetSymbol() && board.GetMap(5, 1) == cp->GetSymbol())) { totalPoints++; }
	cp->SetPoint(totalPoints);

	//std::cout << "\ntotalPoints: " << totalPoints;
	return totalPoints;
}

//Penggunaan fungsi untuk mengecheck apakah board telah penuh
int GameManager::CheckFull()
{
	int filled = 0;
	for (int i = 0; i <= 5; i++)
	{
		for (int j = 0; j <= 5; j++)
		{
			if (board.GetMap(i, j) == p1.GetSymbol() || board.GetMap(i, j) == p2.GetSymbol()) { filled++; /*std::cout << "\n" << i << ',' << j;*/ }
		}
	}

	if (filled >= 25) {
		//std::cout << "\nfilled: " << filled;
		isPlaying = false;
	}
	return filled;
}

//Penggunaan fungsi untuk mengecheck apakah game sudah berjalan
bool GameManager::GetIsPlaying()
{
	return isPlaying;
}


//Get Round
int GameManager::GetRound()
{
	return round;
}
